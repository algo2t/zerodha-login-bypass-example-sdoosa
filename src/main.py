import json
from config import getUserConfig, getServerConfig, getSystemConfig
from flask import Flask, render_template, request, redirect
from zerodha import loginZerodha, getKite
from algo import startAlgo, stopAlgo
import logging
import threading
import time
from instruments import fetchInstruments, getInstrumentDataBySymbol

app = Flask(__name__)
app.config['DEBUG'] = True


def initLoggingConfig():
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")


@app.route('/', methods=['GET'])
def home():
    if 'loggedIn' in request.args and request.args['loggedIn'] == 'true':
        return render_template('index_loggedin.html')
    elif 'algoStarted' in request.args and request.args['algoStarted'] == 'true':
        return render_template('index_algostarted.html')
    else:
        return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login_broker():
    userid = request.form.get('userid')
    password = request.form.get('password')
    twofa = request.form.get('twofapin')
    return loginZerodha(userid, password, twofa)


@app.route('/apis/algo/start', methods=['POST'])
def start_algo():
    x = threading.Thread(target=startAlgo)
    x.start()
    systemConfig = getSystemConfig()
    homeUrl = systemConfig['homeUrl'] + '?algoStarted=true'
    logging.info('Sending redirect url %s in response', homeUrl)
    respData = {'redirect': homeUrl}
    return json.dumps(respData)


@app.route('/apis/algo/stop', methods=['POST'])
def stop_algo():
    # x = threading.Thread(target=stopAlgo)
    # x.start()
    stopAlgo()
    systemConfig = getSystemConfig()
    homeUrl = systemConfig['homeUrl'] + '?loggedIn=true'
    logging.info('Sending redirect url %s in response', homeUrl)
    respData = {'redirect': homeUrl}
    return json.dumps(respData)


@app.route('/positions', methods=['GET'])
def positions():
    kite = getKite()
    positions = kite.positions()
    print('getKite positions => ', positions)
    return json.dumps(positions)


@app.route('/holdings', methods=['GET'])
def holdings():
    kite = getKite()
    holdings = kite.holdings()
    print('getKite holdings => ', holdings)
    return json.dumps(holdings)

@app.route('/profile', methods=['GET'])
def profile():
    kite = getKite()
    profile = kite.profile()
    print('getKite profile => ', profile)
    return json.dumps(profile)


# Execution starts here
initLoggingConfig()

serverConfig = getServerConfig()
logging.info('serverConfig => %s', serverConfig)

userConfig = getUserConfig()
logging.info('userConfig => %s', userConfig)
port = serverConfig['port']
   

if __name__ == "__main__":
    app.run('localhost', port)
