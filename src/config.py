import json


def getServerConfig():
    with open('config/server.json', 'r') as server:
        return json.load(server)


def getSystemConfig():
    with open('config/system.json', 'r') as system:
        return json.load(system)


def getUserConfig():
    with open('config/user.json', 'r') as user:
        return json.load(user)
