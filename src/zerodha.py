import logging
from kiteext import KiteExt
from config import getUserConfig, getSystemConfig
from flask import redirect
from instruments import fetchInstruments
import threading

kite = None
accessToken = None


def getKite():
    return kite


def getAccessToken():
    return accessToken


def loginZerodha(userid, password, twofa):
    systemConfig = getSystemConfig()
    global kite
    global accessToken
    kite = KiteExt()
    kite.login_with_credentials(userid, password, twofa)
    logging.info(kite.profile())
    accessToken = kite.enctoken+"&user_id="+kite.user_id
    logging.info('accessToken = %s', accessToken)
    logging.info('Login successful. accessToken = %s', accessToken)
    # redirect to home page with query param loggedIn=true
    homeUrl = systemConfig['homeUrl'] + '?loggedIn=true'
    logging.info('Redirecting to home page %s', homeUrl)
    return redirect(homeUrl, code=302)
