# Zerodha Login Bypass Example Sdoosa

Implementation of bypass with kiteext for sdoosa code using python flask template. No need to store your credentials.

This project is mainly for newbies into algo trading who are interested in learning to code their own trading algo using python interpreter.

This is in the context of Indian stock exchanges and brokers.

## Pre-requisites

1. Install latest `python` version `3.9.x` this is tested on `python` `3.9.7`
2. Make sure git is installed - you can use [scoop.sh](https://scoop.sh) to install latest git using `scoop install git`
3. Recommended to use virtualenv to have less impact on your system python setup.

## Installing `kiteext` using below command

```console
python -m pip install git+https://gitlab.com/algo2t/kiteext.git
```

## Installing `Flask`

```console
python -m pip install flask
```

## This is very basic project and there is no logout functionality.
Integrate this with new repository of sdoosa from github.com.


